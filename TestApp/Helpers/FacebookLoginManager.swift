//
//  FacebookLoginManager.swift
//  TestApp
//
//  Created by Chamikara R on 7/16/18.
//  Copyright © 2018 EM. All rights reserved.
//

import FacebookLogin
import FacebookCore
import RxSwift

class FacebookLoginManager {
    
    static var shareInstance = FacebookLoginManager()
    
    private  let accessTokenKey = "AccessToken_Facebook"
    
    func logIn(_ fromViewController:UIViewController) -> Observable<Bool> {
        return Observable.create{ observer in
            
            let loginManager = LoginManager()
            loginManager.logIn(readPermissions: [.publicProfile], viewController: fromViewController, completion: { loginResult in
                switch loginResult {
                case .failed(let error):
                    if let error = error as NSError? {
                        let errorDetails = [NSLocalizedDescriptionKey : "Processing Error. Please try again!"]
                        let customError = NSError(domain: "Error!", code: error.code, userInfo: errorDetails)
                        observer.onError(customError)
                    } else {
                        observer.onError(error)
                    }
                    print(error)
                case .cancelled:
                    loginManager.logOut()
                    let errorDetails = [NSLocalizedDescriptionKey : "Request cancelled!"]
                    let customError = NSError(domain: "Request cancelled!", code: 1001, userInfo: errorDetails)
                    observer.onError(customError)
                    print("User cancelled login.")
                case .success( _, _, let accessToken):
                    print("Logged in!")
                    let strAuthenticationToken = accessToken.authenticationToken
                    UserDefaults.standard.set(strAuthenticationToken, forKey: self.accessTokenKey)
                    observer.onNext(true)
                }
            })
            
            return Disposables.create()
        }
    }
    
    func getUserName() -> Observable<String> {
        return Observable.create{ observer in
            
            var accessToken = AccessToken.current
            
            if accessToken == nil || UserDefaults.standard.string(forKey: self.accessTokenKey) != nil {
                accessToken = AccessToken(authenticationToken: UserDefaults.standard.string(forKey: "AccessToken_Facebook")!)
            }
            
            if accessToken != nil {
                let graphRequest: GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields" : "name"], accessToken: accessToken, httpMethod: GraphRequestHTTPMethod.GET, apiVersion: GraphAPIVersion.defaultVersion)
                graphRequest.start { (response, result) in
                    switch result {
                    case .success(let resultResponse):
                        print(resultResponse)
                        observer.onNext(resultResponse.dictionaryValue?["name"] as! String)
                    case .failed(let error):
                        observer.onError(error)
                        print(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func logOut() {
        LoginManager().logOut()
        AccessToken.current = nil
        UserDefaults.standard.removeObject(forKey: accessTokenKey)
    }
}
