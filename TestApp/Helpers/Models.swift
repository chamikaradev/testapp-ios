//
//  Models.swift
//  TestApp
//
//  Created by Chamikara R on 7/17/18.
//  Copyright © 2018 EM. All rights reserved.
//

import Foundation

struct Hotel: Decodable {
    let id: Int
    var title: String
    var description: String
    var address: String
    var postcode: String
    var phoneNumber: String
    var latitude: String
    var longitude: String
    var image: Image
}

 struct Image: Decodable {
    var small: String
    var medium: String
    var large: String
}
