//
//  RootVIewSwitcher.swift
//  TestApp
//
//  Created by Chamikara R on 7/17/18.
//  Copyright © 2018 EM. All rights reserved.
//

import Foundation
import UIKit

class RootViewSwitcher {
    
    fileprivate static let key_loginStatus = "loginStatus"
    
    static func setRootView(view: RootViewController) {
        switch view {
        case .login:
            UserDefaults.standard.set(false, forKey: key_loginStatus)
        case .listView:
            UserDefaults.standard.set(true, forKey: key_loginStatus)
        }
        updateRootVC()
    }
    
    static func updateRootVC() {
        let status = UserDefaults.standard.bool(forKey: key_loginStatus)
        let rootVC = status ?
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListNavigationController")
            : UIStoryboard(name: "Authentication", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = rootVC
    }
    
}

enum RootViewController {
    case login
    case listView
}
