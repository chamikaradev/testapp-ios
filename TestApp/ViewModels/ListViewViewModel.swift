//
//  ListViewViewModel.swift
//  TestApp
//
//  Created by Chamikara R on 7/17/18.
//  Copyright © 2018 EM. All rights reserved.
//

import RxSwift
import SwiftyJSON

class ListViewViewModel {
    
    var hotels = Variable<[Hotel]>([])
    let webService = WebService.shareInstance
    private var disposeBag = DisposeBag()

    func getHotels() -> Observable<[Hotel]>  {
        return Observable.create{ observer in
            var hotels: [Hotel] = []
            _ = WebService.shareInstance.getRequest().subscribe(onNext:{ json in
                print("====> JSON: \(json)")
                if json["status"].intValue == 200 {
                    
                    do {
                        
                        for index in 0...json["data"].arrayValue.count - 1 {
                            let strJson = json["data"].arrayValue[index].rawString()
                            if let data = strJson?.data(using: .utf8) {
                                let object = try JSONDecoder().decode(Hotel.self, from: data)
                                print("Description \(object.image.medium)")
                                hotels.append(object)
                                observer.onNext(hotels)
                            }
                        }
                        
                        let strJson = json["data"].arrayValue[0].rawString()
                        if let data = strJson?.data(using: .utf8) {
                            let object = try JSONDecoder().decode(Hotel.self, from: data)
                            print("Description \(object.description)")
                            hotels.append(object)
                            observer.onNext(hotels)
                        }
                    } catch {
                        print(error)
                    }
                }else{
                    let error = NSError(domain:json["code"].stringValue, code:-404, userInfo:nil)
                    observer.onError(error)
                }
                
            }, onError: { error in
                let err = error as NSError
                let code = String(err.code)
                let errorValue = NSError(domain:code, code:-404, userInfo:nil)
                observer.onError(errorValue)
            }).disposed(by: self.disposeBag)
            
            return Disposables.create()
        }
    }
    
}
