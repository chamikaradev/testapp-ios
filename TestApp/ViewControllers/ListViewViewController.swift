//
//  ListViewViewController.swift
//  TestApp
//
//  Created by Chamikara R on 7/17/18.
//  Copyright © 2018 EM. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class ListViewViewController: BaseViewController {
    
    @IBOutlet weak var btnLogOut: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel = ListViewViewModel()
    
    var hotels = Variable<[Hotel]>([])
    
    var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
        setupNavigationController()
        setupTableView()
        
        getHotels()
        getFacebookName()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func setupView () {
        btnLogOut.rx.tap.subscribe({_ in self.logOut()}).disposed(by: disposeBag)
    }
    
    private func setupNavigationController(){
        navigationController?.navigationBar.barTintColor = UIColor.theme.primary
    }
    
    private func getHotels() {
        showActivityIndicator(uiView: self.view)
        _ = viewModel.getHotels().subscribe(onNext:{ hotels in
            self.hotels.value = hotels
            self.hideActivityIndicator()
        }, onError: { error in
            self.hideActivityIndicator()
            self.alert(title: "Error", message: error.localizedDescription)
        }).disposed(by: disposeBag)
    }
    
    private func getFacebookName() {
        _ = FacebookLoginManager.shareInstance.getUserName().subscribe(onNext:{ userName in
            self.lblUserName.text = userName
            
        }, onError: { error in
            self.alert(title: "Error", message: error.localizedDescription)
        }).disposed(by: disposeBag)
    }
    
    private func setupTableView() {
        hotels.asObservable().bind(to: tableView.rx.items(cellIdentifier: "cell")) { index, model, cell in
            let hotelCell = cell as? ListViewCell
            hotelCell?.setData(hotel: model)
            }
            .disposed(by: disposeBag)
        
        tableView.rx.modelSelected(Hotel.self)
            .subscribe(onNext: { [weak self] item in
                self?.pushToDetailsVC(item: item)
            }).disposed(by: disposeBag)
    }
    
    private func pushToDetailsVC(item: Hotel){
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController {
            viewController.hotel = item
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    private func logOut(){
        FacebookLoginManager.shareInstance.logOut()
        let storyboard: UIStoryboard = UIStoryboard(name: "Authentication", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(vc, animated: true, completion: nil)
        
        RootViewSwitcher.setRootView(view: .login)

    }

}
