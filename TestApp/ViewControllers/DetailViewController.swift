//
//  DetailViewController.swift
//  TestApp
//
//  Created by Chamikara R on 7/17/18.
//  Copyright © 2018 EM. All rights reserved.
//

import UIKit
import MapKit
import Nuke

class DetailViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var hotelTitle: UILabel!
    @IBOutlet weak var hotelDescription: UITextView!
    
    var hotel: Hotel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Details"
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(#imageLiteral(resourceName: "ic_location"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(goToMapView), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let hotel = self.hotel {
            hotelTitle.text = hotel.title
            hotelDescription.text = hotel.description
            let convertedString = hotel.image.large.replacingOccurrences(of: "http", with: "https", options: .literal, range: nil)
            let url = URL(string: convertedString)!
            Nuke.loadImage(with: url, into: imageView)
        }
        
    }
    
    @objc private func goToMapView() {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapViewController") as? MapViewController {
            let latitude = (hotel!.latitude as NSString).doubleValue
            let longitude = (hotel!.longitude as NSString).doubleValue
            viewController.location = getLocation(lat: latitude, long: longitude)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func getLocation(lat:Double, long:Double) -> CLLocation {
        return CLLocation(latitude: lat, longitude: long)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
