//
//  LoginViewController.swift
//  TestApp
//
//  Created by Chamikara R on 7/16/18.
//  Copyright © 2018 EM. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: BaseViewController {

    @IBOutlet weak var btnLogin: UIButton!
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func setupView () {
        btnLogin.rx.tap.subscribe({_ in self.loginWithFacebook()}).disposed(by: disposeBag)
    }
    
    // MARK:login function
    private func loginWithFacebook(){
        showActivityIndicator(uiView: self.view)
        
        FacebookLoginManager.shareInstance.logIn(self).subscribe(onNext:{ isSuccess in
            if isSuccess {
                self.loginSuccess()
                RootViewSwitcher.setRootView(view: .listView)
            }
            self.hideActivityIndicator()
        }, onError: { error in
            self.hideActivityIndicator()
            self.alert(title: "Error", message: error.localizedDescription)
        }).disposed(by: disposeBag)
    }
    
    private func loginSuccess() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ListViewViewController") as! ListViewViewController
        let navController = UINavigationController(rootViewController: vc)
        self.present(navController, animated: true, completion: nil)
    }


}
