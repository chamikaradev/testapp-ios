//
//  BaseViewController.swift
//  TestApp
//
//  Created by Chamikara R on 7/17/18.
//  Copyright © 2018 EM. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var container: UIView = UIView()
    var loaderImageView:UIImageView? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showActivityIndicator(uiView: UIView) {
        if loaderImageView == nil {
            container.frame = uiView.frame
            container.center = uiView.center
            var loaderImage:UIImage = UIImage()
            container.backgroundColor = UIColor.white.withAlphaComponent(0.5)
            loaderImage = #imageLiteral(resourceName: "spinner")
            loaderImageView = UIImageView(image: loaderImage)
            loaderImageView?.contentMode = .scaleAspectFill
            loaderImageView?.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
            loaderImageView?.center = CGPoint(x: container.frame.size.width / 2, y: container.frame.size.height / 2)
            
            container.addSubview(loaderImageView!)
            uiView.addSubview(container)
            loaderImageView?.rotateAnimation()
        }
    }
    
    func hideActivityIndicator() {
        loaderImageView?.removeFromSuperview()
        loaderImageView = nil
        container.removeFromSuperview()
    }
    
    func alert(title:String, message: String) {
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alertViewController.addAction(okButton)
        present(alertViewController, animated: true, completion: nil)
    }

}

private extension UIView {
    func rotateAnimation(duration: CFTimeInterval = 0.9) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 2)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount=Float.infinity
        self.layer.add(rotateAnimation, forKey: nil)
    }
}
