//
//  MapViewController.swift
//  TestApp
//
//  Created by Chamikara R on 7/17/18.
//  Copyright © 2018 EM. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    var location: CLLocation? = nil
    private let newPin = MKPointAnnotation()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Map"
        dropPin()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func dropPin() {
        mapView.removeAnnotation(newPin)
        
        if self.location != nil {
            let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            mapView.setRegion(region, animated: true)
            
            newPin.coordinate = location!.coordinate
            mapView.addAnnotation(newPin)
        }
        
        
    }

}
