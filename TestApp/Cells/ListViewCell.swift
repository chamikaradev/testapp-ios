//
//  ListViewCell.swift
//  TestApp
//
//  Created by Chamikara R on 7/17/18.
//  Copyright © 2018 EM. All rights reserved.
//

import UIKit
import Kingfisher
import Nuke

class ListViewCell: UITableViewCell {

    @IBOutlet weak var hotelImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var hotelDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(hotel: Hotel) {
        title.text = hotel.title
        hotelDescription.text = hotel.description
        
        let convertedString = hotel.image.medium.replacingOccurrences(of: "http", with: "https", options: .literal, range: nil)
        let url = URL(string: convertedString)!
        Nuke.loadImage(with: url, into: hotelImage)
        
    }

}
