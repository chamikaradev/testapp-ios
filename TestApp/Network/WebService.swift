//
//  WebService.swift
//  TestApp
//
//  Created by Chamikara R on 7/17/18.
//  Copyright © 2018 EM. All rights reserved.
//

import RxSwift
import Alamofire
import SwiftyJSON

class WebService {
    static var shareInstance = WebService()
    
    func getRequest() -> Observable<JSON> {
        let url = "https://dl.dropboxusercontent.com/s/6nt7fkdt7ck0lue/hotels.json"
        return Observable.create{ observer in
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default)
                .responseJSON { response in
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value {
                            print("Response:\(data)")
                            observer.onNext(JSON(data))
                        }
                        break
                    case .failure(_):
                        if let error = response.result.error {
                            print("Error:\(error)")
                            observer.onError(error)
                        }
                        break
                    }
            }
            return Disposables.create()
        }
    }
}
